package br.com.mauda.seminario.cientificos.model.enums;

public enum SituacaoInscricaoEnum {

    DISPONIVEL(1L, "Disponível"),
    COMPRADO(2L, "Comprado"),
    CHECKIN(3L, "Check-In");

    private Long id;
    private String nome;

    SituacaoInscricaoEnum(final Long id, final String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Long getId() {
        return this.id;
    }

    public String getNome() {
        return this.nome;
    }
}
