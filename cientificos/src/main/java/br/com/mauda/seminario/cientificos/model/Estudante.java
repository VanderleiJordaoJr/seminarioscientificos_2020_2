package br.com.mauda.seminario.cientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Estudante implements Serializable {

    private static final long serialVersionUID = -8585414729338835213L;

    private Long id;
    private String nome;
    private String telefone;
    private String email;

    private Instituicao instituicao;
    private List<Inscricao> inscricoes = new ArrayList<>();

    public Estudante(final Instituicao instituicao) {
        this.instituicao = instituicao;
    }

    public void adicionarInscricao(final Inscricao inscricao) {
        this.inscricoes.add(inscricao);

    }

    public boolean possuiInscricao(final Inscricao inscricao) {
        return this.inscricoes.contains(inscricao);

    }

    public void removerInscricao(final Inscricao inscricao) {
        this.inscricoes.remove(inscricao);
    }

    public Long getId() {
        return this.id;
    }

    public String getNome() {
        return this.nome;
    }

    public String getTelefone() {
        return this.telefone;
    }

    public String getEmail() {
        return this.email;
    }

    public Instituicao getInstituicao() {
        return this.instituicao;
    }

    public List<Inscricao> getInscricoes() {
        return this.inscricoes;
    }

    public void setNome(final String nome) {
        this.nome = nome;
    }

    public void setTelefone(final String telefone) {
        this.telefone = telefone;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }

        final Estudante other = (Estudante) obj;
        if (this.id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!this.id.equals(other.id)) {
            return false;
        }
        return this.hashCode() == other.hashCode();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.id == null ? 0 : this.id.hashCode());
        return result;
    }
}
