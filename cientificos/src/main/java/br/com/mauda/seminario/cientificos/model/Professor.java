package br.com.mauda.seminario.cientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Professor implements Serializable {

    private static final long serialVersionUID = 3439626219669565422L;

    private Long id;
    private String nome;
    private String telefone;
    private String email;
    private Double salario;

    private List<Seminario> seminarios = new ArrayList<>();
    private Instituicao instituicao;

    public Professor(final Instituicao instituicao) {
        this.instituicao = instituicao;
    }

    public void adicionarSeminario(final Seminario seminario) {
        this.seminarios.add(seminario);
    }

    public boolean possuiSeminario(final Seminario seminario) {
        return this.seminarios.contains(seminario);
    }

    public Long getId() {
        return this.id;
    }

    public String getNome() {
        return this.nome;
    }

    public String getTelefone() {
        return this.telefone;
    }

    public String getEmail() {
        return this.email;
    }

    public Double getSalario() {
        return this.salario;
    }

    public List<Seminario> getSeminarios() {
        return this.seminarios;
    }

    public Instituicao getInstituicao() {
        return this.instituicao;
    }

    public void setNome(final String nome) {
        this.nome = nome;
    }

    public void setTelefone(final String telefone) {
        this.telefone = telefone;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public void setSalario(final Double salario) {
        this.salario = salario;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }

        final Professor other = (Professor) obj;
        if (this.id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!this.id.equals(other.id)) {
            return false;
        }
        return this.hashCode() == other.hashCode();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.id == null ? 0 : this.id.hashCode());
        return result;
    }

}
