package br.com.mauda.seminario.cientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AreaCientifica implements Serializable {

    private static final long serialVersionUID = 5118906756068573949L;

    private Long id;
    private String nome;
    private List<Curso> cursos;

    public AreaCientifica() {
        this.cursos = new ArrayList<>();
    }

    public void adicionarCurso(final Curso curso) {
        this.cursos.add(curso);
    }

    public boolean possuiCurso(final Curso curso) {
        return this.cursos.contains(curso);
    }

    public Long getId() {
        return this.id;
    }

    public String getNome() {
        return this.nome;
    }

    public List<Curso> getCursos() {
        return this.cursos;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }

        final AreaCientifica other = (AreaCientifica) obj;
        if (this.id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!this.id.equals(other.id)) {
            return false;
        }
        return this.hashCode() == other.hashCode();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.id == null ? 0 : this.id.hashCode());
        return result;
    }
}
