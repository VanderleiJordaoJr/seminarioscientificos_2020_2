package br.com.mauda.seminario.cientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Seminario implements Serializable {

    private static final long serialVersionUID = 6518459043450431402L;

    private Long id;
    private String titulo;
    private String descricao;
    private Boolean mesaRedonda;
    private Date data;
    private Integer qtdInscricoes;

    private List<Inscricao> inscricoes = new ArrayList<>();
    private List<AreaCientifica> areas = new ArrayList<>();
    private List<Professor> professores = new ArrayList<>();

    public Seminario(final AreaCientifica areaCientifica, final Professor professor, final Integer qtdInscricoes) {
        this.areas.add(areaCientifica);
        this.professores.add(professor);
        professor.adicionarSeminario(this);
        this.qtdInscricoes = qtdInscricoes;
        this.criarInscricoes();
    }

    public void adicionarAreaCientifica(final AreaCientifica areaCientifica) {
        this.areas.add(areaCientifica);
    }

    public void adicionarInscricao(final Inscricao inscricao) {
        this.inscricoes.add(inscricao);
    }

    public void adicionarProfessor(final Professor professor) {
        this.professores.add(professor);
    }

    public boolean possuiAreaCientifica(final AreaCientifica areaCientifica) {
        return this.areas.contains(areaCientifica);
    }

    public boolean possuiInscricao(final Inscricao inscricao) {
        return this.inscricoes.contains(inscricao);
    }

    public boolean possuiProfessor(final Professor professor) {
        return this.professores.contains(professor);
    }

    public Long getId() {
        return this.id;
    }

    public String getTitulo() {
        return this.titulo;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public Boolean getMesaRedonda() {
        return this.mesaRedonda;
    }

    public Date getData() {
        return this.data;
    }

    public Integer getQtdInscricoes() {
        return this.qtdInscricoes;
    }

    public List<Inscricao> getInscricoes() {
        return this.inscricoes;
    }

    public List<AreaCientifica> getAreasCientificas() {
        return this.areas;
    }

    public List<Professor> getProfessores() {
        return this.professores;
    }

    public void setTitulo(final String titulo) {
        this.titulo = titulo;
    }

    public void setDescricao(final String descricao) {
        this.descricao = descricao;
    }

    public void setMesaRedonda(final Boolean mesaRedonda) {
        this.mesaRedonda = mesaRedonda;
    }

    public void setData(final Date data) {
        this.data = data;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }

        final Seminario other = (Seminario) obj;
        if (this.id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!this.id.equals(other.id)) {
            return false;
        }
        return this.hashCode() == other.hashCode();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.id == null ? 0 : this.id.hashCode());
        return result;
    }

    private void criarInscricoes() {
        for (int i = 0; i < this.getQtdInscricoes(); i++) {
            new Inscricao(this);
        }
    }
}
