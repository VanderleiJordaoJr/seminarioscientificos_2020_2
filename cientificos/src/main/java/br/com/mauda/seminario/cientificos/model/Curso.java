package br.com.mauda.seminario.cientificos.model;

import java.io.Serializable;

public class Curso implements Serializable {

    private static final long serialVersionUID = 7931856785085451979L;

    private Long id;
    private String nome;
    private AreaCientifica areaCientifica;

    public Curso(final AreaCientifica areaCientifica) {
        this.areaCientifica = areaCientifica;
        this.areaCientifica.adicionarCurso(this);
    }

    public Long getId() {
        return this.id;
    }

    public String getNome() {
        return this.nome;
    }

    public AreaCientifica getAreaCientifica() {
        return this.areaCientifica;
    }

    public void setNome(final String nome) {
        this.nome = nome;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }

        final Curso other = (Curso) obj;
        if (this.id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!this.id.equals(other.id)) {
            return false;
        }
        return this.hashCode() == other.hashCode();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.id == null ? 0 : this.id.hashCode());
        return result;
    }

}
